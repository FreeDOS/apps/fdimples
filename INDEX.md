# FDIMPLES

FreeDOS Installer - My Package List Editor Software|(FDIMPLES) is the package list editor for the advanced mode of the FreeDOS Installer (FDI). It also can run as a stand-alone program for a more user friendly interface to the FDINST command line package management utility.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## FDIMPLES.LSM

<table>
<tr><td>title</td><td>FDIMPLES</td></tr>
<tr><td>version</td><td>0.11.8</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-10-01</td></tr>
<tr><td>description</td><td>Easily install and remove packages</td></tr>
<tr><td>summary</td><td>FreeDOS Installer - My Package List Editor Software|(FDIMPLES) is the package list editor for the advanced mode of the FreeDOS Installer (FDI). It also can run as a stand-alone program for a more user friendly interface to the FDINST command line package management utility.</td></tr>
<tr><td>keywords</td><td>dos 16 bit, asm, pascal</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://fd.lod.bz/repos/current/pkg-html/fdimples.html</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://github.com/shidel/FDIMPLES</td></tr>
<tr><td>original&nbsp;site</td><td>http://up.lod.bz/FDIMPLES/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
