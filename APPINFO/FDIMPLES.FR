Begin3
Language:    FR, 850, French (Standard)
Title:       FDIMPLES
Description: Installer et enlever des paquets facilement
Summary:     Programme d'installation de FreeDOS - Logiciel d'�dition de la liste de mes paquets|FDIMPLES est l'�diteur de liste de paquets pour le mode avanc� du programme d'installation de FreeDOS (FDI). Il peut �galement fonctionner en tant que programme autonome pour fournir une interface plus conviviale � l'utilitaire de gestion de paquets en ligne de commande FDINST.
Keywords:    dos 16 bits, asm, pascal
End
